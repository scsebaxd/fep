<?php

$usuario = $_GET['usuario'];
$id = $_GET['id'];
$Nempresa = $_GET['Nempresa'];
$servicio = $_GET['servicio'];
$Eid = $_GET['Eid'];

$server = "mysql";
$user = "root";
$password = "pass";
$database = "login";
$conn = mysqli_connect($server, $user, $password, $database);
$sql = "SELECT * from empresa WHERE id=$id";
$result = mysqli_query($conn, $sql);
$mostrar = mysqli_fetch_array($result);

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/empresa/style_regi.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>productos</title>
</head>

<body>
    <form class='cuadrogeneral' action="<?php echo "R_atencion_registro.php?id=$id&servicio=$servicio&usuario=$usuario&Nempresa=$Nempresa&Eid=$Eid"?>" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <td>
                    <div class='centrar1'>
                        <img class="p-1"src='/IMG/Image 21-2022-10-14+01_49_32.790874/Raster_9/assets/Raster.png' alt width='150'
                            height='150'>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class='centrar1 cuadro8 negro bg-info'>
                        <h3>
                            <strong>
                                <?php echo $Nempresa ?>
                            </strong>
                        </h3>
                    </div>
                </td>
            </tr>
        </table><br>


        <div class="card align-items-lg-center  ">

            <img src='/IMG/<?php echo $servicio ?>.png' alt width='50' height='50'>

            <div class="custom-control custom-checkbox text-dark">
                <label class="custom-control-label" for="customCheck1">
                    <h4>
                        <strong>
                            <?php echo $servicio ?>
                        </strong>
                    </h4>
                </label>

            </div>
        </div><br>
        <div class="card cuadro1  bg-secondary ">

            <h5>Seleccione Fecha y Hora </h5>

        </div>
        <table class="card header align-items-center p-2 ">
            <tr>
                <td>
                    <div class="align-items-lg-center">
                        <div class="custom-control custom-checkbox">
                            <label class="custom-control-label">fecha:</label>
                            <input type="date" name="fecha" required>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="align-items-lg-center">
                        <div class="custom-control custom-checkbox">
                            <label class="custom-control-label">Hora :</label>
                            <input type="time" name="hora" required>

                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <div class="card cuadro1 align-items-center bg-secondary">
            <h5>Indiquenos tu Nombre </h5>
        </div>
        <div class="card cuadro1 p-2 align-items-center">
            <input type="text" placeholder="Escriba el nombre de la persona que llevara la mascota..."
                name="Nombre"required>
        </div>
        <div class="card cuadro1 align-items-center bg-secondary">
           <h5>Numero de contacto </h5> 
        </div>
        <div class="card cuadro1 p-2 align-items-center ">   
            <input type="text" placeholder="Escriba su numero telefonico..."
                name="numero"required>
        </div>
        <div class="card cuadro1 align-items-center bg-secondary">
          <h5>Descripcion del problema </h5>  
        </div>
        <div class="card cuadro1 p-2 align-items-center">     
            <textarea class="form-control" rows="5" id="comment" name="descripcion"required></textarea>
        </div>
        
        <br>

        <input class='btn btn-success cuadro9' type='submit' value='Guardar'></input>

    </form>

    <div id="inferior" class="btn-group btn-group-lg">

        <?php echo "<a href='/css/usuario/tiket.php?usuario=$usuario&id=$id' type='button'  class='btn btn-secondary' >---Ticket---
                <img src='/IMG/tiket.png' alt='' width='40' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/inicio_user.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'> --Inicio--
                <img src='/IMG/home.png' alt='' width='30' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/perfil.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'>--Cuenta--
            <img src='/IMG/perfil.png' alt='' width='30' height='30' class='raster'>
        </a>"; ?>
    </div>
</body>

</html>