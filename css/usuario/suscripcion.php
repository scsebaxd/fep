<?php
$usuario = $_GET['usuario'];
$id = $_GET['id'];
$server = "mysql";
$user = "root";
$password = "pass";
$database = "login";
include("db.php");
$conn = mysqli_connect($server, $user, $password, $database);


?>
<script type="text/javascript">
    function mostrarPassword() {
        var cambio = document.getElementById("txtPassword");
        if (cambio.type == "password") {
            cambio.type = "text";
            $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        } else {
            cambio.type = "password";
            $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }
    }

    $(document).ready(function () {
        //CheckBox mostrar contraseña
        $('#ShowPassword').click(function () {
            $('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
        });
    });
</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/empresa/style_regi.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>productos</title>
</head>

<body>

    <div class="cuadrogeneral" action="registro_productos.php" method="post" enctype="multipart/form-data">
        <strong>
            <h1 class="bg-dark ">Cuenta</h1>
        </strong><br>
        <br>
        <table>
            <tr>
                <td>
                    <div class="centrar1"><img src="/IMG/foto_perfil.png" alt width="150" height="150"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="centrar1 cuadro8 negro">
                        <h3><strong>Marcelo Valenzuela</strong></h3>
                    </div>
                </td>
            </tr>
        </table>

        <br>
        <TABLE class="centrar2">
            <TR>
                <TD>
                    <div class="cuadro4 negro"><br>
                        <h4><strong>Suscripción</strong></h4>
                    </div>
                </TD>
                <td>
                    <div class="cuadro5"><img src="/IMG/suscripcion.png" alt width="75" height="75">
                    </div>
                </td>
            </TR>
        </TABLE>
        <?php
        $sql2 = "SELECT * from usuarios where id='$id'";
        $result2 = mysqli_query($conn, $sql2);
        $mostrar2 = mysqli_fetch_array($result2);
        if($mostrar2['suscripcion'] == 3) {
        ?>
        <table>
            <tr class="centrar3">
                <br>
                <TD>
                    <div class="cuadro10 negro"><br>
                        <h4><strong>Panda</strong></h4><img src="/IMG/pandarojo.png" width="100" height="105">
                        <h6>$ 5.400 2 ofertas de alimentos 2 descuentos para atencion veterinaria oferta en juguetes
                            o accesorios personalizacion de perfil</h6>
                    </div>
                </TD>
            </tr>
        </TABLE>
        <?php
        }
        if($mostrar2['suscripcion'] == 2) {
            ?>
            <table>
                <tr class="centrar3">
                    <br>
                    <TD>
                        <div class="cuadro10 negro"><br>
                            <h4><strong>Koala</strong></h4><img src="/IMG/koala.png" width="100" height="105">
                            <h6>$ 4.300 2 oferta de alimentos 1 atencion medica en veterinaria 1 juguetes y accesorios personalizacion de perfil</h6>
                        </div>
                    </TD>
                </tr>
            </TABLE>
            <?php
            }
            if($mostrar2['suscripcion'] == 1) {
                ?>
                <table>
                    <tr class="centrar3">
                        <br>
                        <TD>
                            <div class="cuadro10 negro"><br>
                                <h4><strong>Hamster</strong></h4><img src="/IMG/hamster.png" width="100" height="105">
                                <h6>$ 2.500 1 oferta de alimentos 1 atencion medica en veterina personalizacion de perfil</h6>
                            </div>
                        </TD>
                    </tr>
                </TABLE>
                <?php
                }
        ?>
        <br>
        <h3><strong>Codigos Disponibles</strong></h3><br>
        <?php
        $sql = "SELECT * from codigo_dinamico where id_usuario_tiket = '$id' ";
        $result = mysqli_query($conn, $sql);
        while ($mostrar = mysqli_fetch_array($result)) {
        ?>
        <form id="form1 ">
            <div class="col-md-6 ">
            </div>
            <div class="input-group cuadro7 negro">
                <input type='Password' ID='txtPassword' name='Contraseña' class='form-control'
                    placeholder='codigo dinamico...' value="<?php echo $mostrar['codigo']; ?>" readonly>
                <button id='show_password ' class='btn btn-primary' type='button' onclick='mostrarPassword()'> <span
                        class='fa fa-eye-slash icon'></span>
                    <svg id=clickme width=28 height=24 xmlns='http://www.w3.org/2000/svg' viewBox='0 0 576 512'>
                        <path
                            d='M569.354 231.631C512.97 135.949 407.81 72 288 72 168.14 72 63.004 135.994 6.646 231.631a47.999 47.999 0 0 0 0 48.739C63.031 376.051 168.19 440 288 440c119.86 0 224.996-63.994 281.354-159.631a47.997 47.997 0 0 0 0-48.738zM288 392c-102.556 0-192.091-54.701-240-136 44.157-74.933 123.677-127.27 216.162-135.007C273.958 131.078 280 144.83 280 160c0 30.928-25.072 56-56 56s-56-25.072-56-56l.001-.042C157.794 179.043 152 200.844 152 224c0 75.111 60.889 136 136 136s136-60.889 136-136c0-31.031-10.4-59.629-27.895-82.515C451.704 164.638 498.009 205.106 528 256c-47.908 81.299-137.444 136-240 136z' />
                    </svg>
                </button>
                </input>
            </div>
        </form><br>
        <?php
        }
            ?>
    </div>

    <div id="inferior" class="btn-group btn-group-lg">

        <?php echo "<a href='/css/usuario/tiket.php?usuario=$usuario&id=$id' type='button'  class='btn btn-secondary' >---Ticket---
                <img src='/IMG/tiket.png' alt='' width='40' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/inicio_user.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'> --Inicio--
                <img src='/IMG/home.png' alt='' width='30' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/perfil.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'>--Cuenta--
            <img src='/IMG/perfil.png' alt='' width='30' height='30' class='raster'>
        </a>"; ?>
    </div>
</body>

</html>