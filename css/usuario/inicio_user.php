<?php
if ($id == "") {
    $usuario = $_GET['usuario'];
    $id = $_GET['id'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/empresa/style_regi.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Inicio</title>
</head>

<body>

    <div class="cuadrogeneral">
        <img class="izqu" src="/IMG/Image 21-2022-10-14+01_49_32.790874/Raster_9/assets/Raster.png" alt="" width="95"
            height="95">
        <div class="cuadro_1 negro text-success">
            <h2><strong>
                    <?php echo ($usuario) ?>
                </strong> 
        </div>
        <div>
            <h6></h6>
        </div>
        <strong>
            <h1 class="bg-dark animate__animated animate__backInLeft">Tienda: Animales y Mascotas</h1>
        </strong>



        <div class="list-group">
            <TABLE CELLSPACING="5" WIDTH="514" height="514">
                <TR>
                    <?php echo "<TD><img src='/IMG/casaycama.png' width='150' height='130'><a href='/css/usuario/2pg.php?usuario=$usuario&id=$id' class='list-group-item list-group-item-action'>Ver Casas y camas</a></TD> <TD><img src='/IMG/higiene.png' width='150' height='130'> <a href='/css/usuario/2pg.php?usuario=$usuario&id=$id' class='list-group-item list-group-item-action'>Ver Utiles de Higiene</a> </TD>"; ?>
                </TR>
                <TR>
                    <?php echo "<TD><img src='/IMG/alimento.png' width='150' height='130'> <a href='/css/usuario/2pg.php?usuario=$usuario&id=$id' class='list-group-item list-group-item-action'>Ver Alimentos</a></TD> <TD><img src='/IMG/jugutes_accesorios.png' width='150' height='130'><a href='/css/usuario/2pg.php?usuario=$usuario&id=$id' class='list-group-item list-group-item-action'>Ver Juguetes y Accesorios</a> </TD>"; ?>
                </TR>
            </TABLE>
        </div>


    </div>
    <div id="inferior" class="btn-group btn-group-lg">

        <?php echo "<a href='/css/usuario/tiket.php?usuario=$usuario&id=$id' type='button'  class='btn btn-secondary' >---Ticket---
                <img src='/IMG/tiket.png' alt='' width='40' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/inicio_user.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'> --Inicio--
                <img src='/IMG/home.png' alt='' width='30' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/perfil.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'>--Cuenta--
            <img src='/IMG/perfil.png' alt='' width='30' height='30' class='raster'>
        </a>"; ?>
    </div>

</body>

</html>