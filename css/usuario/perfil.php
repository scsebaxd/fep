<?php
$usuario = $_GET['usuario'];
$id = $_GET['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/empresa/style_regi.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>productos</title>
</head>

<body>
    <div class="cuadrogeneral" method="post" enctype="multipart/form-data">
        <strong>
            <h1 class="bg-dark ">Cuenta <a href='<?php echo "soporte_tecnico.php?usuario=$usuario&id=$id"?>'><img src='/IMG/info.png' alt='' width='30'
                        height='30' class='raster'></a></h1>
        </strong><br>
        <br>
        <table>
            <tr>
                <td>
                    <div class="centrar1"><img src="/IMG/foto_perfil.png" alt width="150" height="150"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo "<div class='centrar1 cuadro8 negro'><h3><strong> $usuario </strong></h3>
                    
                    </div>"; ?>

                </td>
            </tr>
        </table>
        <br>
        <br>
        <TABLE class="centrar2">
            <TR>
                <?php echo "<TD><a href='/css/usuario/mis_datos.php?usuario=$usuario&id=$id'class=' list-group-item-action ' ><div class='cuadro4'><br><h4><strong>Mis datos</strong></h4></div>
                </a>
                </TD>"; ?>
                <td>
                    <div class="cuadro5"><img src="/IMG/Foto_datos.png" alt width="80" height="80">
                    </div>
                </td>
            </TR>
            <tr>
                <TD><br>
                    <?php echo "<a href='/css/usuario/seguridad.php?usuario=$usuario&id=$id'' class=' list-group-item-action '>
                        <div class='cuadro4'><br>
                            <h4><strong>Seguridad</strong></h4>
                        </div>
                    </a>"; ?>
                </TD>
                <td><br>
                    <div class="cuadro5"><img src="/IMG/seguridad.png" alt width="80" height="80">
                    </div>
                </td>
                </TD>
            </tr>
            <tr>
                <TD><br>
                <a href="<?php echo "/css/usuario/suscripcion.php?usuario=$usuario&id=$id"; ?>" class=" list-group-item-action ">
                        <div class="cuadro4"><br>
                            <h4><strong>Suscripción</strong></h4>
                        </div>
                    </a>
                </TD>
                <td><br>
                    <div class="cuadro5"><img src="/IMG/suscripcion.png" alt width="80" height="80">
                    </div>
                </td>
                </TD>
            </TR>
            </TR>
        </TABLE>
    </div>
    <div id="inferior" class="btn-group btn-group-lg">

        <?php echo "<a href='/css/usuario/tiket.php?usuario=$usuario&id=$id' type='button'  class='btn btn-secondary' >---Ticket---
                <img src='/IMG/tiket.png' alt='' width='40' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/inicio_user.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'> --Inicio--
                <img src='/IMG/home.png' alt='' width='30' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/perfil.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'>--Cuenta--
            <img src='/IMG/perfil.png' alt='' width='30' height='30' class='raster'>
        </a>"; ?>
    </div>
</body>

</html>