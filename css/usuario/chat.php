<?php
$usuario = $_GET['usuario'];
$id = $_GET['id'];

?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/empresa/style_regi.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Soporte Tecnico</title>
</head>

<body>


    <div class="cuadrogeneral">
        <img class="izqu" src="/IMG/perrooo.png" alt="" width="70" height="100">
        <div class="cuadro_1 negro">
            <h2><strong>Soporte Tecnico</strong></h2>
        </div>
        <div>
            <h6></h6>
        </div>
        <strong>
            <h1 class="bg-dark ">Rellene todo los datos para contactarte :)</h1>
        </strong>

        <br>

        <div class="list-group">

            <h3><label class='negro'><strong>¿A que celular quieres que te escribamos?</strong></label></h3><br>
            <input type='text' placeholder='escriba su numero con +569' name='numero'><br>
            <input class='btn btn-success' name='Guardar' type='submit' value='Enviar'>
            <br>
            <h4 class="text-success">Te contactaremos por wasap</h4>

        </div>

    </div>
    <div id="inferior" class="btn-group btn-group-lg">
        <?php echo "<a href='/css/usuario/tiket.php?usuario=$usuario&id=$id' type='button'  class='btn btn-secondary' >---Ticket---
          <img src='/IMG/tiket.png' alt='' width='40' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/inicio_user.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'> --Inicio--
            <img src='/IMG/home.png' alt='' width='30' height='30' class='raster'>
            </a>"; ?>
        <?php echo "<a href='/css/usuario/perfil.php?usuario=$usuario&id=$id' type='button' class='btn btn-secondary'>--Cuenta--
            <img src='/IMG/perfil.png' alt='' width='30' height='30' class='raster'>
            </a>"; ?>
    </div>

</body>

</html>